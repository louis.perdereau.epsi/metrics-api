var daemon = require("daemonize2").setup({
    main: "server.js",
    name: "API",
    pidfile: "server_api.pid"
});

switch (process.argv[2]) {

    case "start":
        daemon.start();
        break;

    case "stop":
        daemon.stop();
        break;
    
    case "restart":
        daemon.stop();
        daemon.start();
        break;

    default:
        console.log("Usage: [start|stop|restart]");
}