const mongo = require('../mongo');

function getHostnames(){
  return mongo.CLIENT.then(db => {
    return db
      .db(mongo.DB)
      .collection("metrics")
      .distinct("hostname");
  })
}

module.exports = {
  getHostnames: getHostnames,
}