const metricsController = require('./controller')
const validators = require('./validators')

module.exports = [{
    method: "GET",
    path: "/metrics/{hostname}",
    handler: metricsController.handleGetMetrics,
    options: {
      description: "Get metrics from a specific hostname",
      notes: "Returns from 1 to 20 metrics from a specific hostname",
      tags: ["api"],
      validate: validators.getMetricsFromHostname
    }
  },
  {
    method: "POST",
    path: "/metrics/{hostname}",
    handler: metricsController.handlePostMetrics,
    options: {
      description: "Insert metrics for a specific hostname",
      notes: "Returns HTTP message",
      tags: ["api"],
      auth: {
        strategies: ['keycloak-jwt']
      },
      validate: validators.postMetricsFromHostname
    }
  }]