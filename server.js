var os = require("os-utils");
const HapiSwagger = require("hapi-swagger");
const Inert = require("@hapi/inert");
const Vision = require("@hapi/vision");
const Pack = require("./package");
const Hapi = require("@hapi/hapi");
const authKeycloak = require('hapi-auth-keycloak');

const API_PORT = process.env.API_PORT || 3000;
const API_HOST = process.env.API_HOST || 'localhost';

const KEYCLOAK_PROTOCOL = process.env.KEYCLOAK_PROTOCOL || 'http';
const KEYCLOAK_DOMAIN = process.env.KEYCLOAK_DOMAIN || '';
const KEYCLOAK_REALM = process.env.KEYCLOAK_REALM || '';
const KEYCLOAK_CLIENT_ID = process.env.KEYCLOAK_CLIENT_ID || '';
const KEYCLOAK_CLIENT_SECRET = process.env.KEYCLOAK_CLIENT_SECRET || '';

const KEYCLOAK_URL = `${KEYCLOAK_PROTOCOL}://${KEYCLOAK_DOMAIN}/auth/realms/${KEYCLOAK_REALM}`


init = async () => {
  const server = Hapi.server({
    port: API_PORT,
    host: API_HOST,
    routes: {
        validate: {
            failAction: (request, h, err) => {

                throw err;
            }
        }
    }
  });

  const swaggerOptions = {
    info: {
      title: 'Test API Documentation',
      description: 'This is a sample example of API documentation.',
      version: Pack.version,
    },
    securityDefinitions: {
      jwt: {
          type: 'apiKey',
          name: 'Authorization',
          in: 'header'
      }
    },
    security: [{ 'jwt': [] }],
  };

  const authPluginOptions = {}

  const authStrategyOptions = {
    realmUrl: KEYCLOAK_URL,
    clientId: KEYCLOAK_CLIENT_ID,
    secret: KEYCLOAK_CLIENT_SECRET,
    userInfo: ['name', 'email']
  };

  await server.register([
    Inert,
    Vision,
    {
      plugin: authKeycloak,
      options: authPluginOptions
    },  
    {
      plugin: HapiSwagger,
      options: swaggerOptions
    },
  ])

  server.auth.strategy('keycloak-jwt', 'keycloak-jwt', authStrategyOptions);

  server.route(require('./hostnames/routes'));

  server.route(require('./metrics/routes'));

  await server.start();
  console.log("Server running on %s", server.info.uri);
};

process.on("unhandledRejection", err => {
  console.log(err);
  process.exit(1);
});

init();
